/**
*	Defines booking module
*
*	@author Mehrosh Sadia
*/
define([
	'angular',
	'core/module'
], function( angular ){
	var module = angular.module('BookFlight.booking', [
		'BookFlight.core'
	]);

	return module;
});